#include <stdio.h>

#include "thread.h"
#include "xtimer.h"

/* Add lps331ap related include here */
#include "lps331ap.h"
#include "lps331ap_params.h"

/* Add lsm303dlhc related include here */
#include "lsm303dlhc.h"
#include "lsm303dlhc_params.h"

/* Declare the lps331ap device variable here */
static lps331ap_t lps331ap;

/* Declare the lsm303dlhc device variable here */
static lsm303dlhc_t lsm303dlhc;

static char stack[THREAD_STACKSIZE_MAIN];

static void *thread_handler(void *arg)
{
    (void)arg;

    /* Add the lsm303dlhc sensor polling endless loop here */
    while (1) {
        lsm303dlhc_3d_data_t mag_value;
        lsm303dlhc_3d_data_t acc_value;
        lsm303dlhc_read_acc(&lsm303dlhc, &acc_value);
        printf("Accelerometer x: %i y: %i z: %i\n",
               acc_value.x_axis, acc_value.y_axis, acc_value.z_axis);
        lsm303dlhc_read_mag(&lsm303dlhc, &mag_value);
        printf("Magnetometer x: %i y: %i z: %i\n",
               mag_value.x_axis, mag_value.y_axis, mag_value.z_axis);
        xtimer_usleep(500 * US_PER_MS);
    }
    
    return 0;
}

int main(void)
{
    /* Initialize the lps331ap sensor here */
    lps331ap_init(&lps331ap, &lps331ap_params[0]);
    
    /* Initialize the lsm303dlhc sensor here */
    lsm303dlhc_init(&lsm303dlhc, lsm303dlhc_params);
    
    thread_create(stack, sizeof(stack), THREAD_PRIORITY_MAIN - 1,
               0, thread_handler, NULL, "lsm303dlhc");
    
    /* Add the lps331ap sensor polling endless loop here */
     while (1) {
        uint16_t pres = 0;
        int16_t temp = 0;
        lps331ap_read_temp(&lps331ap, &temp);
        lps331ap_read_pres(&lps331ap, &pres);
        printf("Pressure: %uhPa, Temperature: %u.%u°C\n",
               pres, (temp / 100), (temp % 100));
        xtimer_sleep(2);
    }
    
    return 0;
}
